unit TerminatedDemoThread;

interface

uses System.Classes,
     FMX.StdCtrls;

type
  TCountUntilTerminatedThread = class(TThread)
  private
    FLabel :TLabel;
    Fi     :Integer;
  protected
    procedure Execute; override; public
    constructor Create(aLabel: TLabel);
    property i :Integer read Fi write Fi;
  end;

implementation

uses System.SysUtils,
     System.SyncObjs;

{ TCountUntilTerminatedThread }
constructor TCountUntilTerminatedThread.Create(aLabel: TLabel);
begin
   inherited Create(True);
   FLabel := aLabel;
end;

procedure TCountUntilTerminatedThread.Execute;
begin
   inherited;
   while not Terminated do begin
       Synchronize(procedure
                   begin
                      FLabel.Text := Fi.ToString;
                   end);
       Sleep(1000);
       TInterlocked.Increment(Fi);
   end;
end;

end.
