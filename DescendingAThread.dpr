program DescendingAThread;

uses
  System.StartUpCopy,
  FMX.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  VerySimpleThread in 'VerySimpleThread.pas',
  MemoMessageThread in 'MemoMessageThread.pas',
  SlowCode in 'SlowCode.pas',
  TerminatedDemoThread in 'TerminatedDemoThread.pas',
  ExceptionThread in 'ExceptionThread.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
