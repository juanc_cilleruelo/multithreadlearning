unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, System.Threading,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo,
  MemoMessageThread,
  TerminatedDemoThread,
  ExceptionThread;

type
  ETaskException = class(Exception);

  TForm1 = class(TForm)
    BtnRunThread: TButton;
    BtnCreateSuspended: TButton;
    BtnStartMemoMessageThread: TButton;
    Memo1: TMemo;
    GroupBox1: TGroupBox;
    BtnStartThread: TButton;
    BtnTerminateThread: TButton;
    LabelNumber: TLabel;
    GroupBox2: TGroupBox;
    BtnStartExceptionThread: TButton;
    GroupBox3: TGroupBox;
    BtnStartAnonymousThread: TButton;
    BtnSyncroniceAnonymousThread: TButton;
    BtnQueueAnonymousThread: TButton;
    GroupBox4: TGroupBox;
    BtnWithoutTask: TButton;
    BtnWithTask: TButton;
    GroupBox5: TGroupBox;
    BtnRunThreeTasks: TButton;
    BtnWaitForAllTasks: TButton;
    BtnWaitForAnyTask: TButton;
    BtnWaitForTheFirstTwo: TButton;
    GroupBox6: TGroupBox;
    BtnRunThreeTasksFutures: TButton;
    BtnWaitForAnyFuture: TButton;
    BtnWaitForAllFutures: TButton;
    GroupBox7: TGroupBox;
    BtnCreatingATask: TButton;
    BtnExecutingATask: TButton;
    GroupBox8: TGroupBox;
    BtnRunTaskWithException: TButton;
    BtnRunATaskWithTryExcept: TButton;
    BtnAcquiringException: TButton;
    GroupBox9: TGroupBox;
    BtnStartALongTask: TButton;
    BtnCancelTheLongTask: TButton;
    GroupBox10: TGroupBox;
    BtnCountFrom1To10: TButton;
    BtnPrimesBelow: TButton;
    BtnButNowResponsive: TButton;
    BtnCancelLongTimeLoop: TButton;
    BtnLongTimeParallelFor: TButton;
    procedure BtnRunThreadClick(Sender: TObject);
    procedure BtnStartMemoMessageThreadClick(Sender: TObject);
    procedure BtnCreateSuspendedClick(Sender: TObject);
    procedure BtnStartThreadClick(Sender: TObject);
    procedure BtnTerminateThreadClick(Sender: TObject);
    procedure BtnStartExceptionThreadClick(Sender: TObject);
    procedure BtnStartAnonymousThreadClick(Sender: TObject);
    procedure BtnSyncroniceAnonymousThreadClick(Sender: TObject);
    procedure BtnQueueAnonymousThreadClick(Sender: TObject);
    procedure BtnWithoutTaskClick(Sender: TObject);
    procedure BtnWithTaskClick(Sender: TObject);
    procedure BtnRunThreeTasksClick(Sender: TObject);
    procedure BtnWaitForAllTasksClick(Sender: TObject);
    procedure BtnWaitForAnyTaskClick(Sender: TObject);
    procedure BtnWaitForTheFirstTwoClick(Sender: TObject);
    procedure BtnRunThreeTasksFuturesClick(Sender: TObject);
    procedure BtnWaitForAllFuturesClick(Sender: TObject);
    procedure BtnWaitForAnyFutureClick(Sender: TObject);
    procedure BtnCreatingATaskClick(Sender: TObject);
    procedure BtnExecutingATaskClick(Sender: TObject);
    procedure BtnRunTaskWithExceptionClick(Sender: TObject);
    procedure BtnRunATaskWithTryExceptClick(Sender: TObject);
    procedure BtnAcquiringExceptionClick(Sender: TObject);
    procedure BtnStartALongTaskClick(Sender: TObject);
    procedure BtnCancelTheLongTaskClick(Sender: TObject);
    procedure BtnCountFrom1To10Click(Sender: TObject);
    procedure BtnPrimesBelowClick(Sender: TObject);
    procedure BtnButNowResponsiveClick(Sender: TObject);
    procedure BtnLongTimeParallelForClick(Sender: TObject);
    procedure BtnCancelLongTimeLoopClick(Sender: TObject);
  private
    FMemoMessageThread :TMemoMessageThread;
    FThread            :TCountUntilTerminatedThread;
    FExceptionThread   :TExceptionThread;
    FAllTasks          :array[0..2] of ITask;
    {----------------------------------------}
    FResult200000      :IFuture<Integer>;
    FResult300000      :IFuture<Integer>;
    FResult400000      :IFuture<Integer>;
    FFutures           :array[0..2] of ITask;
    {----------------------------------------}
    FTask              :ITask;
    {----------------------------------------}
    FForLoopTask: ITask;
    {----------------------------------------}
    procedure HandleTermination(Sender: TObject);
    procedure HandleException(Sender: TObject);
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses System.Diagnostics,
     VerySimpleThread, SlowCode;

procedure TForm1.BtnCreateSuspendedClick(Sender: TObject);
begin
   FMemoMessageThread := TMemoMessageThread.Create(Memo1);
   FMemoMessageThread.MemoMessage := 'Hello from the TMemoMessageThread thread';
end;

procedure TForm1.BtnRunThreadClick(Sender: TObject);
begin
   TVerySimpleThread.Create(Memo1, 'This comes from the', ' secondary thread.');
   Memo1.Lines.Add('This comes from the main thread');
end;


procedure TForm1.BtnStartMemoMessageThreadClick(Sender: TObject);
begin
   FMemoMessageThread.Start;
   FMemoMessageThread := nil;
end;

procedure TForm1.BtnStartThreadClick(Sender: TObject);
begin
   FThread := TCountUntilTerminatedThread.Create(LabelNumber);
   FThread.FreeOnTerminate := True;
   FThread.OnTerminate := HandleTermination;
   FThread.Start;
end;

procedure TForm1.HandleTermination(Sender: TObject);
begin
   LabelNumber.Text := 'Ended at ' + FThread.i.ToString;
end;

procedure TForm1.BtnTerminateThreadClick(Sender: TObject);
begin
   FThread.Terminate;
end;

{ Exception Thread }

procedure TForm1.HandleException(Sender: TObject);
var LThread :TExceptionThread;
begin
   if Sender is TExceptionThread then begin
      LThread := TExceptionThread(Sender);
   end
   else begin
      Memo1.Lines.Add('Sender is not a TExceptionThread');
      Exit;
   end;

   if LThread.FatalException <> nil then begin
      if LThread.FatalException is Exception then begin
         // Cast to Exception needed because FatalException is a TObject
         Memo1.Lines.Add(Exception(LThread.FatalException).Message);
      end;
   end;
end;

procedure TForm1.BtnStartExceptionThreadClick(Sender: TObject);
begin
   FExceptionThread := TExceptionThread.Create(Memo1);
   FExceptionThread.OnTerminate := HandleException;
   FExceptionThread.Start;
end;

{ Anonymous Thread }

procedure TForm1.BtnStartAnonymousThreadClick(Sender: TObject);
var LTotal :Integer;
begin
   TThread.CreateAnonymousThread(procedure
                                 begin
                                    LTotal := PrimesBelow(150000);
                                    TThread.Synchronize(TThread.CurrentThread,
                                    procedure
                                    begin
                                       Memo1.Lines.Add('Total: ' + LTotal.ToString);
                                       Memo1.Lines.Add('All Done from an Anonymous thread');
                                    end);
                                 end).Start;
end;

procedure TForm1.BtnSyncroniceAnonymousThreadClick(Sender: TObject);
var LTotal :Integer;
begin
   TThread.Synchronize(TThread.CurrentThread, procedure
                                              begin
                                                 LTotal := PrimesBelow(150000);
                                                 Memo1.Lines.Add('Total: ' + LTotal.ToString);
                                                 Memo1.Lines.Add('All done from the Synchronize class procedure');
                                              end);
end;

procedure TForm1.BtnQueueAnonymousThreadClick(Sender: TObject);
var LTotal: integer;
begin
   TThread.Queue(TThread.CurrentThread, procedure
                                        begin
                                           LTotal := PrimesBelow(150000);
                                           Memo1.Lines.Add('Total: ' + LTotal.ToString);
                                           Memo1.Lines.Add('All Done from the Queue class procedure');
                                        end);
end;

{ Without and with task }

procedure TForm1.BtnWithoutTaskClick(Sender: TObject);
var Stopwatch      :TStopWatch;
    Total          :Integer;
    ElapsedSeconds :Double;
begin
   Stopwatch := TStopwatch.StartNew;
   Total := PrimesBelow(200000);
   ElapsedSeconds := StopWatch.Elapsed.TotalSeconds;
   Memo1.Lines.Add(Format('There are  %d primes under 200,000', [Total]));
   Memo1.Lines.Add(Format('It took %:2f seconds to calculate that', [ElapsedSeconds]));
end;


procedure TForm1.BtnWithTaskClick(Sender: TObject);
begin
   TTask.Run(procedure
             var Stopwatch      :TStopWatch;
                 Total          :Integer;
                 ElapsedSeconds :Double;
             begin
                Stopwatch := TStopwatch.StartNew;
                Total     := PrimesBelow(200000);
                ElapsedSeconds := StopWatch.Elapsed.TotalSeconds;
                TThread.Synchronize(nil, procedure
                                         begin
                                            Memo1.Lines.Add(Format('There are %d' + 'primes under 200,000', [Total]));
                                            Memo1.Lines.Add(Format('It took %:2f seconds to calculate that', [ElapsedSeconds]));
                                         end);
             end);
end;

{ Multiple Tasks Demo }

procedure TForm1.BtnRunThreeTasksClick(Sender: TObject);
begin
   FAllTasks[0] := TTask.Run(procedure
                             begin
                                PrimesBelow(200000);
                                TThread.Synchronize(TThread.Current,
                                                    procedure
                                                    begin
                                                       Memo1.Lines.Add('200000 is done');
                                                    end);
                             end);

   FAllTasks[1] := TTask.Run(procedure
                             begin
                                PrimesBelow(300000);
                                TThread.Synchronize(TThread.Current,
                                                    procedure
                                                    begin
                                                       Memo1.Lines.Add('300000 is done');
                                                    end);
                             end);

   FAllTasks[2] := TTask.Run(procedure
                             begin
                                PrimesBelow(400000);
                                TThread.Synchronize(TThread.Current,
                                                    procedure
                                                    begin
                                                       Memo1.Lines.Add('400000 is done');
                                                    end);
                             end);
end;

procedure TForm1.BtnWaitForAllTasksClick(Sender: TObject);
begin
   TTask.Run(procedure
             begin
                TTask.WaitForAll(FAllTasks);
                TThread.Synchronize(nil,
                                    procedure
                                    begin
                                       Memo1.Lines.Add('All Done');
                                    end);
             end);
end;

procedure TForm1.BtnWaitForAnyTaskClick(Sender: TObject);
begin
   TTask.Run(procedure
             begin
                TTask.WaitForAny(FAllTasks);
                TThread.Synchronize(nil,
                                    procedure
                                    begin
                                       Memo1.Lines.Add('At least one is Done');
                                    end);
             end);
end;

procedure TForm1.BtnWaitForTheFirstTwoClick(Sender: TObject);
begin
   TTask.Run(procedure
             var SomeTasks: array[0..1] of ITask;
             begin
                SomeTasks[0] := FAllTasks[0];
                SomeTasks[1] := FAllTasks[1];
                TTask.WaitForAll(SomeTasks);
                TThread.Synchronize(nil,
                                    procedure
                                    begin
                                       Memo1.Lines.Add('The First Two are Done');
                                    end);
             end);
end;

procedure TForm1.BtnRunThreeTasksFuturesClick(Sender: TObject);
begin
   FResult200000 := TTask.Future<Integer>(function:Integer
                                           begin
                                              Result := PrimesBelow(200000);
                                           end);
   Memo1.Lines.Add('200000 Started');
   FFutures[0] := FResult200000;


   FResult300000 := TTask.Future<Integer>(function:Integer
                                          begin
                                             Result := PrimesBelow(300000);
                                          end);
   Memo1.Lines.Add('300000 Started');
   FFutures[1] := FResult300000;

   FResult400000 := TTask.Future<Integer>(function:Integer
                                          begin
                                             Result := PrimesBelow(400000);
                                          end);
   Memo1.Lines.Add('400000 Started');
   FFutures[2] := FResult400000;
end;

procedure TForm1.BtnWaitForAllFuturesClick(Sender: TObject);
begin
   TFuture<Integer>.WaitForAll(FFutures);
   Memo1.Lines.Add('Done Waiting. This should appear with all the results.');
   Memo1.Lines.Add('There are ' + FResult200000.GetValue.ToString + ' prime numbers under 200,000');
   Memo1.Lines.Add('There are ' + FResult300000.GetValue.ToString + ' prime numbers under 300,000');
   Memo1.Lines.Add('There are ' + FResult400000.GetValue.ToString + ' prime numbers under 400,000');
end;

procedure TForm1.BtnWaitForAnyFutureClick(Sender: TObject);
begin
   Memo1.Lines.Add('There are ' + FResult200000.GetValue.ToString + ' prime numbers under 200,000');
   Memo1.Lines.Add('Done Waiting. This should appear with the first result.');
   Memo1.Lines.Add('There are ' + FResult300000.GetValue.ToString + ' prime numbers under 300,000');
   Memo1.Lines.Add('There are ' + fResult400000.GetValue.ToString + ' prime numbers under 400,000');
end;

{ Creating a task without executing it inmediatelly }

procedure TForm1.BtnCreatingATaskClick(Sender: TObject);
begin
   FTask := TTask.Create(procedure
                         var Num :Integer;
                         begin
                            Num := PrimesBelow(200000);
                            TThread.Synchronize(nil,
                                                procedure
                                                begin
                                                   Memo1.Lines.Add('There are ' + Num.ToString() + ' primes below 200000');
                                                end);
                         end);
end;

procedure TForm1.BtnExecutingATaskClick(Sender: TObject);
begin
   FTask.Start;
end;

{ Exception Handling }

procedure TForm1.BtnRunTaskWithExceptionClick(Sender: TObject);
begin
   TTask.Run(procedure
             var
             Stopwatch      :TStopWatch;
             Total          :Integer;
             ElapsedSeconds :Double;
             begin
                Stopwatch := TStopwatch.StartNew;
                // oops, something happened!
                raise Exception.Create('An error of some sort occurred in the task');
                Total          := PrimesBelow(200000);
                ElapsedSeconds := StopWatch.Elapsed.TotalSeconds;
                TThread.Synchronize(nil, procedure
                                         begin
                                            Memo1.Lines.Add(Format('There are %d primes under 200,000'     , [Total]));
                                            Memo1.Lines.Add(Format('It took %:2f seconds to calcluate that', [ElapsedSeconds]));
                                         end);
             end);
end;

procedure TForm1.BtnRunATaskWithTryExceptClick(Sender: TObject);
begin
   TTask.Run(procedure
             var
             Stopwatch      :TStopWatch;
             Total          :Integer;
             ElapsedSeconds :Double;
             begin
                try
                   Stopwatch := TStopwatch.StartNew;
                   // oops, something happened!
                   raise ETaskException.Create('An error of some sort occurred in the task');
                   Total          := PrimesBelow(200000);
                   ElapsedSeconds := StopWatch.Elapsed.TotalSeconds;
                   TThread.Synchronize(nil, procedure
                                            begin
                                               Memo1.Lines.Add(Format('There are %d primes under 200,000'     , [Total]));
                                               Memo1.Lines.Add(Format('It took %:2f seconds to calcluate that', [ElapsedSeconds]));
                                            end);
                except
                   on e: ETaskException do begin
                      TThread.Queue(TThread.CurrentThread, procedure
                                                           begin
                                                              raise E;
                                                           end);
                end;

                end;
             end);
end;

procedure TForm1.BtnAcquiringExceptionClick(Sender: TObject);
var AcquiredException :Exception;
begin
   TTask.Run(procedure
             var
             Stopwatch      :TStopWatch;
             Total          :Integer;
             ElapsedSeconds :Double;
             begin
                try
                   Stopwatch := TStopwatch.StartNew;
                   // oops, something happened!
                   raise ETaskException.Create('An error of some sort occurred in the task');
                   Total          := PrimesBelow(200000);
                   ElapsedSeconds := StopWatch.Elapsed.TotalSeconds;
                   TThread.Synchronize(nil, procedure
                                            begin
                                               Memo1.Lines.Add(Format('There are %d primes under 200,000'     , [Total]));
                                               Memo1.Lines.Add(Format('It took %:2f seconds to calcluate that', [ElapsedSeconds]));
                                            end);
                except
                   on e: ETaskException do begin
                      AcquiredException := ETaskException(System.AcquireExceptionObject);
                      TThread.Queue(TThread.CurrentThread, procedure
                                                           begin
                                                              raise AcquiredException;
                                                           end);
                   end;
                end;
             end);
end;

{ Stopping a long task }

procedure TForm1.BtnStartALongTaskClick(Sender: TObject);
begin
   FTask := TTask.Run(procedure
                      var i :Integer;
                      begin
                         for i := 1 to 100000 do begin
                            Sleep(1000);
                            if TTask.CurrentTask.Status = TTaskStatus.Canceled then begin
                               Exit;
                            end;
                            TThread.Queue(TThread.CurrentThread, procedure
                                                                 begin
                                                                    Memo1.Lines.Add(i.ToString());
                                                                 end);
                         end;
                      end).Start;
end;

procedure TForm1.BtnCancelTheLongTaskClick(Sender: TObject);
begin
   FTask.Cancel;
end;

{ TParallel.For demo }

procedure TForm1.BtnCountFrom1To10Click(Sender: TObject);
begin
   Memo1.Lines.Clear;
   TParallel.&For(1, 10, procedure (aIndex :Integer)
                         begin
                            PrimesBelow(50000);
                            TThread.Queue(TThread.CurrentThread, procedure
                                                                 begin
                                                                    Memo1.Lines.Add(aIndex.ToString());
                                                                 end);
                         end);
end;

procedure TForm1.BtnPrimesBelowClick(Sender: TObject);
var PrimeTotal :Integer;
    TotalTime  :Integer;
    StopWatch  :TStopWatch;
    S          :string;
begin
   StopWatch  := TStopWatch.StartNew;
   PrimeTotal := PrimesBelow(100000);
   StopWatch.Stop;
   TotalTime := StopWatch.ElapsedMilliseconds;
   S := Format('There are %d primes below 100,000.  It took %d milliseconds to figure that out.', [PrimeTotal, TotalTime]);
   Memo1.Lines.Add(S);
   Memo1.Lines.Add('');
   StopWatch := TStopWatch.StartNew;

   PrimeTotal := PrimesBelowParallel(100000);
   StopWatch.Stop;
   TotalTime := StopWatch.ElapsedMilliseconds;
   S := Format('There are %d primes below 100,000.  It took %d milliseconds to figure that out.', [PrimeTotal, TotalTime]);
   Memo1.Lines.Add(S);
end;

procedure TForm1.BtnButNowResponsiveClick(Sender: TObject);
var LoopResult :TParallel.TLoopResult;
begin
   Memo1.Lines.Clear;
   TTask.Run(procedure
             begin
                LoopResult := TParallel.&For(1, 30, procedure(aIndex :Integer)
                                                    begin
                                                       PrimesBelow(50000);
                                                       TThread.Queue(TThread.Current, procedure
                                                                                      begin
                                                                                         Memo1.Lines.Add(aIndex.ToString());
                                                                                      end);
                                                    end);
                if LoopResult.Completed then begin
                   Memo1.Lines.Add('The loop completed.')
                end;
             end);

end;

procedure TForm1.BtnLongTimeParallelForClick(Sender: TObject);
var LoopResult :TParallel.TLoopResult;
begin
   Memo1.Lines.Clear;

   FForLoopTask := TTask.Create(procedure
                                begin
                                   LoopResult := TParallel.&For(1, 30, procedure(aIndex :Integer; LoopState :TParallel.TLoopState)
                                                                       begin
                                                                          if (FForLoopTask.Status = TTaskStatus.Canceled) and (not LoopState.Stopped) then begin
                                                                             LoopState.Stop;
                                                                          end;
                                                                          if LoopState.Stopped then begin
                                                                             TThread.Queue(TThread.Current, procedure
                                                                                                            begin
                                                                                                               Memo1.Lines.Add(aIndex.ToString + ' has stopped early');
                                                                                                            end);
                                                                             Exit;
                                                                          end;
                                                                          PrimesBelow(150000);
                                                                          TThread.Queue(TThread.Current, procedure
                                                                                                         begin
                                                                                                            Memo1.Lines.Add(aIndex.ToString());
                                                                                                         end);
                                                                       end);
                                   if LoopResult.Completed then begin
                                      Memo1.Lines.Add('The Loop Completed')
                                   end
                                   else begin
                                      Memo1.Lines.Add('The loop stopped before the end')
                                   end;
                                end);
   FForLoopTask.Start;
end;

procedure TForm1.BtnCancelLongTimeLoopClick(Sender: TObject);
begin
   if Assigned(FForLoopTask) then begin
      FForLoopTask.Cancel;
   end;
end;

end.
