unit SlowCode;

interface

function SlowIsPrime(aInteger: integer): Boolean;
function PrimesBelow(aInteger: integer): integer; var i: integer;
function PrimesBelowParallel(aInteger :Integer):Integer;

implementation

uses System.Threading, System.SyncObjs;


// An incredibly inefficient way to calculate primes
function SlowIsPrime(aInteger: integer): Boolean;
var i :Integer;
begin
   Assert(aInteger > 0);
   // Remember, 1 is not prime
   if aInteger = 1 then begin
      Result := False;
      Exit;
   end;

   Result := True;

   for i := 2 to aInteger - 1 do begin
      if aInteger mod i = 0 then begin
         Result := False;
         Exit;
      end;
   end;
end;

function PrimesBelow(aInteger: integer): integer; var i: integer;
begin
   Result := 0;

   for i := 1 to aInteger do begin
      if SlowIsPrime(i) then begin
         Result := Result + 1;
      end;
   end;
end;

function PrimesBelowParallel(aInteger :Integer):Integer;
var Temp :Integer;
begin
   Temp := 0;
   TParallel.For(1, aInteger, procedure(aIndex :Integer)
                              begin
                                 if SlowIsPrime(aIndex) then begin
                                    TInterlocked.Increment(Temp);
                                 end;
                              end);
   Result := Temp;
end;

end.
